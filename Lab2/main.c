#define FCY 29491200UL

#include <stdio.h>			//standard IO library C
#include <xc.h>
#include <libpic30.h>               //dsPIC30 library
#include <stdlib.h>

//Device configuration registers
#pragma config FPR      = XT_PLL16
#pragma config FOS      = PRI
#pragma config FCKSMEN  = CSW_FSCM_OFF
#pragma config WDT      = WDT_OFF
#pragma config MCLRE    = MCLR_EN
#pragma config FPWRT    = PWRT_OFF

char rxreg;
char reception_flag = 0;

int main(void)
{
    // UART Configuration
    __C30_UART=2;
    U2MODEbits.UARTEN=1;
    U2STAbits.UTXEN=1;
    U2BRG=(unsigned int)191;
    IEC1bits.U2RXIE = 1;
        
    printf("Serial port online\n");
    
    while(1)
    {
        // Receiver block
        while(1){
            if(reception_flag)
                break;
        }             // wait reception
        reception_flag = 0;                 // reset flag
        printf("Received: %c\n", rxreg);    // echo
    }
    return 0;
}

/****************************************************
*													*
*				Other functions						*
*													*
****************************************************/

/* This is UART2 receive ISR */
void __attribute__((__interrupt__,auto_psv)) _U2RXInterrupt(void)
{
   	IFS1bits.U2RXIF = 0;	//resets and re-enables the RX2 interrupt flag
	
	// Read the receive buffer until at least one or more character can be read
    while(U2STAbits.URXDA)
    {   
        rxreg = U2RXREG;
        reception_flag = 1;		
	}
}