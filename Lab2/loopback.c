#define FCY 29491200UL

#include <stdio.h>			//standard IO library C
#include <xc.h>
#include <libpic30.h>               //dsPIC30 library
#include <stdlib.h>

//Device configuration registers
#pragma config FPR      = XT_PLL16
#pragma config FOS      = PRI
#pragma config FCKSMEN  = CSW_FSCM_OFF
#pragma config WDT      = WDT_OFF
#pragma config MCLRE    = MCLR_EN
#pragma config FPWRT    = PWRT_OFF

struct Data
{
    char fifo[80];
    int pos;
    char comms_finished;
};

struct Data data;

void clear_data()
{
    data.comms_finished = 0;
    data.pos = 0;
    data.fifo[0] = 0;
}

int main(void)
{
    
    int i = 0;
    char command_list[3][15] =  {
                                    "Hello\n",
                                    "What's up?\n",
                                    "Goodbye\n"
                                };
        
    // UART Configuration
    __C30_UART=2;
    U2MODEbits.LPBACK = 1;
    U2MODEbits.UARTEN=1;
    U2STAbits.UTXEN=1;
    U2BRG=(unsigned int)191;
    IEC1bits.U2RXIE = 1;
    
    clear_data();
    
    while(1)
    {
        __delay_ms(500);
        
        // Transmitter block
        printf(command_list[i]);
        i++;
        if(i>2) i = 0;
        
        // Receiver block
        while(!data.comms_finished);
        printf("Received: %s\n", data.fifo);
        clear_data();
    }
    return 0;
}

/****************************************************
*													*
*				Other functions						*
*													*
****************************************************/

/* This is UART2 receive ISR */
void __attribute__((__interrupt__,auto_psv)) _U2RXInterrupt(void)
{
    static char dump;
   	IFS1bits.U2RXIF = 0;	//resets and re-enables the RX2 interrupt flag
	
	// Read the receive buffer until at least one or more character can be read
    while(U2STAbits.URXDA)
    {   
        if(data.comms_finished) // Ignore communication
            dump = U2RXREG;
        else
        {
            data.fifo[data.pos] = U2RXREG; // stores the last received char in the buffer
            if(data.fifo[data.pos]=='\n'){  // newline character is end of communication
                data.comms_finished = 1;
                data.fifo[data.pos + 1] = 0;  // add end line character 
                data.pos = 0;            
            }
            else   
                data.pos++; //increments the position in the buffer to store the next char
        }
		
	}
}