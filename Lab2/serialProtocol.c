
#define FCY 29491200L       //instruction frequency

//Library includes
#include <xc.h>
// xc.h finds the correct library to include based on $MCU

#include <libpic30.h>           //C30 compiler definitions
//#include <p30F4011.h>           //defines os dspic registers
#include <stdio.h>			//standart IO library C
#include <uart.h>			//UART (serial port) function and utilities library
#include <timer.h>			//timer library

//Initial Pic Configuration
#pragma config FCKSMEN = CSW_FSCM_OFF //Clock Switching and Monitoring: both disabled
#pragma config FOS = PRI // Oscillator Source: Primary Oscillator
#pragma config FPR = XT_PLL16 //Set the PLL to x16 
#pragma config WDT = WDT_OFF //Watch-dog timer off
#pragma config MCLRE = MCLR_EN //Enable Master Clear
#pragma config FPWRT = PWRT_OFF //Disable Low power

int coef[10]  ;

#define BUFF_LENGHT 80
//Different state of ATM machine
typedef enum
{
    PST_IDLE,
    PST_FIND_START,
    PST_FIND_CMD,
    PST_FIND_LENGTH,
    PST_FIND_DATA,
    PST_FIND_CS,
    PST_FIND_EXEC_CMD,
} ui8ParseState;

ui8ParseState  eSystemState = PST_IDLE;

char RXbuffer[BUFF_LENGHT];	//buffer used to store characters from serial port
unsigned int str_pos = 0; 	//position in the RXbuffer
unsigned int ui8RxCharCount = 0; 
unsigned int N = 0; 	//position in the RXbuffer

void ADC1_Int()  {
    int s;
    IFS0bits.ADIF = 0; //clear AD1IF
    s = 1;//ADCBUF0; //fetch sample
}

void clear_rxbuff(void) {
    int i;
	str_pos = 0;	//returns the pointer to position zero in the circular buffer
	for(i=0; i<BUFF_LENGHT; i++)
        RXbuffer[i] = '\0';		//erases the buffer

}
int exec_command(char c){
    // switch (c))
    return 0;
}
/* main code */
int main(int argc, char** argv) {
	unsigned int UMODEvalue, U2STAvalue; //auxiliary UART config variables 
	
	/*user variables*/
    unsigned int i;
    
	//Configure pin 2 and 3 of port D as outputs -> LEDS
	TRISDbits.TRISD2 = 0;
	TRISDbits.TRISD3 = 0;

	//Configures every PORTB pin as digital I/Os (wont be using analog inputs for now)	
	ADPCFG = 0xFFFF;

	/* Serial port config */
	UMODEvalue = UART_EN & UART_IDLE_CON & UART_NO_PAR_8BIT; //activates the uart in continuos mode (no sleep) and 8bit no parity mode
	U2STAvalue = UART_INT_TX & UART_TX_ENABLE & UART_INT_RX_CHAR & UART_RX_TX; //activates interrupt of pin Tx + enables Tx + enable Rx interrupt for every char
 	OpenUART2 (UMODEvalue, U2STAvalue, 15); //configures and activates UART2 at 115000 bps
	U2STAbits.URXISEL = 1;
	_U2RXIE = 1; 							//0-Interruption off, 1-Interruption on
	U2MODEbits.LPBACK = 0; 					//disables hardware loopback on UART2. Enable only for tests

	__C30_UART = 2; 						//define UART2 as predefined for use with stdio library, printf etc

	printf("\n\rSerial port ONLINE \n"); 	//to check if the serial port is working
    
    clear_rxbuff();
	/* Begin main execution cycle */
	while(1)
	{
		switch(eSystemState){
            case PST_IDLE: 
                if (str_pos != 0){
                    if (RXbuffer[0]  == 0x32)  // '!'
                        eSystemState = PST_FIND_START;
                    else 
                        clear_rxbuff(); // Repeat please
                    RXbuffer[0]  = 0;
                }
            
            break;
            case PST_FIND_START: 
                if (str_pos > 1){
                  switch(RXbuffer[1]){
                    case 'n': 
                        printf(" Command 'n' received\n");		//prints RXbuffer content to the console
                        eSystemState = PST_FIND_CMD;
                    break;
                    case 'f': 
                        printf(" Command 'f' received\n");		//prints RXbuffer content to the console
                        eSystemState = PST_FIND_CMD;
                    break;
                    default:
                        printf(" Command not recognized \n");		//prints RXbuffer content to the console
                        clear_rxbuff(); // Repeat please
                        eSystemState = PST_IDLE;
                  }        
                }        
            break;
            case PST_FIND_CMD: 
               if (str_pos > 2){
                   ui8RxCharCount = 0; 
                   if ((RXbuffer[2] > 0x2F) && (RXbuffer[2] < 0x3A )) {
                       N = RXbuffer[2] - 0x30; // Convert to int
                       if (N >0 ) 
                           eSystemState = PST_FIND_DATA;
                       else 
                           eSystemState = PST_FIND_CS; // Skip data
                    }
                    else {
                        clear_rxbuff(); // Repeat please
                        eSystemState = PST_IDLE;
                    }
            }
            break;
            case PST_FIND_DATA: 
               if (str_pos > (3 + N)){
                   // 	for(i=0; i< N; i++) 

                   eSystemState = PST_FIND_CS; 
               }
            break;
            case PST_FIND_CS: 
               if (str_pos > (3 + N)){
                   // 
                   eSystemState = PST_FIND_EXEC_CMD; 
               }
            break;
            case PST_FIND_EXEC_CMD: 
               if (str_pos > (4 + N)){
                    if ((RXbuffer[4 + N] =='\n') && (RXbuffer[5 + N] =='\r' )){ 
                         exec_command(RXbuffer[1]);
                        printf(" Command executed \n");		//prints RXbuffer content to the console
                    }
                    else 
                        printf(" Command not valid \n");		
                        
                   // 
               }
//             break; // not break go to default 
            default:
                clear_rxbuff(); // Repeat please
               eSystemState = PST_IDLE; // Skip data
      // default statements
        }        
                

	}//end while(1)

}//end main


/****************************************************
*													*
*				Other functions						*
*													*
****************************************************/
		
/* This is UART2 receive ISR */
void __attribute__((__interrupt__,auto_psv)) _U2RXInterrupt(void)
{
   	IFS1bits.U2RXIF = 0;	//resets and reenables the Rx2 interrupt flag
	
	// Read the receive buffer until at least one or more character can be read
    while(U2STAbits.URXDA)
    {
		RXbuffer[str_pos] = U2RXREG;	//stores the last received char in the buffer
		str_pos++;						//increments the position in the buffer to store the next char
		if(str_pos >= 80){str_pos = 0;}	//if the last position is reached then return to initial position
    }
}
